/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.fqueue.demo;

import net.apexes.fqueue.FQueue;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class FQueueDemo2 {

    public static void main(String[] args) throws Exception {
        FQueue queue = new FQueue(".temp/test-fqueue");

        queue.offer("abc".getBytes());
        System.out.println("label1: " + queue.size());

        String data2 = new String(queue.peek());
        System.out.println("label2: " + queue.size());
        System.out.println("label2: data2 -> " + data2);

        String data3 = new String(queue.remove());
        System.out.println("label3: " + queue.size());
        System.out.println("label3: data3 -> " + data3);
    }
}
